#!/usr/bin/env python

import numpy as np
import pandas as pd
import pysam
import sys
from pileup_modules import *
import click

# Import BAM file

@click.command()
@click.option('bamfile', '--input-file', '-i', required=True)
@click.option('reference', '--reference', '-f', required=True)

def calculate_parameters(bamfile,reference):
    samfile = pysam.AlignmentFile(bamfile, "rb",reference_filename=reference)
#samfile = pysam.AlignmentFile("GEN02021_Barret_Manual_P4_A1_markdup.bam", "rb",reference_filename="/well/ludwig/users/hdp075/resources/MergedGenomes/Merged_GRCh38_ERCC_EBV_HP.fa")
reffile = pysam.FastaFile("/well/ludwig/users/hdp075/resources/MergedGenomes/Merged_GRCh38_ERCC_EBV_HP.fa")


# Pileup all positions
# Calculate parameters for each variant

print("CHROM,POS,ALT,DP,AD_ALT,AF,AVG_MAPQ,AVG_BAQ,FS,AVG_nM_distance,NO_PCR_GROUPS,FR_PCR_GROUPS,LABEL")


for position in find_variant_positions(samfile,reffile):
    variants_list = find_variants(samfile,reffile,position[0], position[1])
    for specific_variant_info in variants_list:
        CHROM = specific_variant_info[0]
        POS = specific_variant_info[1]
        ALT = specific_variant_info[2]
        AD_ALT = specific_variant_info[3]
        if AD_ALT >=3: 
            DP = get_coverage(samfile,reffile,specific_variant_info[0], specific_variant_info[1])
            AF = round(float(specific_variant_info[3])/float(DP),2)
            if AF >= 0.01:
                FR_PCR_GROUPS = find_variants_without_duplicates(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])[1]
                if FR_PCR_GROUPS >= 0.01:
                    AVG_MAPQ = get_average_mapping_quality(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])
                    AVG_BAQ = get_average_base_quality(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2]) 
                    FS = fisher_exact_test(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])      
                    AVG_nM_distance = average_mismatches(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2]) 
                    NO_PCR_GROUPS = find_variants_without_duplicates(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])[0] 
                    # Add labels
                    LABEL = add_labels(specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])
                    OUT = [CHROM,POS,ALT,DP,AD_ALT,AF,AVG_MAPQ,AVG_BAQ,FS,AVG_nM_distance,NO_PCR_GROUPS,FR_PCR_GROUPS,LABEL]
                    print ', '.join(map(str, OUT))
        
## potentially add stand info - present on 1 or 2 strands? FS kinda contains this but this could be used for initial filtering 
# vdb,rpb?
# add mapq threshold in the very beginning