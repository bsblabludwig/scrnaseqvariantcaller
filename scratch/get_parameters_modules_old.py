#!/usr/bin/env python

import numpy as np
import pandas as pd
import pysam
import sys


def find_variant_positions(samfile,reffile):
    
    ### Iterates through the alignment and identifies positions with alternative alleles
    ### Output: list of list : [[chrom,pos],[chrom,pos]...]
    
    pileupout = []
    for pileupcolumn in samfile.pileup():
        if pileupcolumn.n >= 3:
            variants = []
            ref_variant = reffile.fetch(pileupcolumn.reference_name,pileupcolumn.pos,pileupcolumn.pos+1)
            a = ref_variant
            for pileupread in pileupcolumn.pileups:
                if a == ref_variant:
                    if not pileupread.is_del and not pileupread.is_refskip:
                        a = pileupread.alignment.query_sequence[pileupread.query_position]
                if a != ref_variant:
                    if "chr" in pileupcolumn.reference_name or "ERC" in pileupcolumn.reference_name:
                        pileupout.append([pileupcolumn.reference_name,pileupcolumn.pos])
                        break                
    return pileupout

    
def find_variants(samfile,reffile,chrom,pos):
    
    ### Finds variant alleles for given genomic position and counts reads supporting them
    ### Output: list of lists [chrom,pos,alt,count] - sometimes multiple variants
    
    variants = []
    output = []
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        ref_variant = reffile.fetch(pileupcolumn.reference_name,pos,pos+1)
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos:
                if not pileupread.is_del and not pileupread.is_refskip:
                    if pileupread.alignment.query_sequence[pileupread.query_position] != ref_variant:
                        variants.append(pileupread.alignment.query_sequence[pileupread.query_position])
    a = variants.count("A")
    c = variants.count("C")
    g = variants.count("G")
    t = variants.count("T")
    if "A" != ref_variant and a != 0:
        output.append([chrom,pos,"A",a])
    if "C" != ref_variant and c != 0:
        output.append([chrom,pos,"C",c])
    if "G" != ref_variant and g != 0:
        output.append([chrom,pos,"G",g])
    if "T" != ref_variant and t != 0:
        output.append([chrom,pos,"T",t])
                
    return output    

def get_coverage(samfile,reffile,chrom,pos): 
    
    ### Finds number of reads covering the region
    
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        if pileupcolumn.pos == pos:
            return pileupcolumn.n  

def get_average_mapping_quality(samfile,reffile,chrom,pos,alt): 
    
    ### Finds average mapping quality of reads supporting the variant
    
    mapq = 0
    count = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos and not pileupread.is_del and not pileupread.is_refskip and pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                mapq += pileupread.alignment.mapping_quality
                count += 1
    return float(mapq)/float(count)
  
def get_average_base_quality(samfile,reffile,chrom,pos,alt): ##IN PROGRESS##
    
    ### Finds average base quality of alternative bases
    
    baq = 0
    count = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos and not pileupread.is_del and not pileupread.is_refskip and pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                baq += pileupread.alignment.query_qualities[pileupread.query_position]
                count += 1
    return float(baq)/float(count)
    
    
def find_variants_without_duplicates(samfile,reffile,chrom,pos,alt):
    
    ### Finds variant alleles for given genomic position and counts reads supporting them - PCR duplicates removed
    ### Output: list with [number of PCR groups present in, % of PCR groups present in (AF with PCR groups removed)]
    
    supporting_reads = 0
    all_reads = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos:
                if not pileupread.is_del and not pileupread.is_refskip and not pileupread.alignment.is_duplicate:
                    all_reads += 1
                    if pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                        supporting_reads += 1 
    af = round(float(supporting_reads)/float(all_reads),2)       
    return [supporting_reads, af]
    
    
def fisher_exact_test(samfile,reffile,chrom,pos,alt):
    
    ### Calculates a p-value for the Fisher Exact Test
    ### Input: [[ref_forward,ref_reverse],[alt_forward,alt_reverse]]
    
    import scipy.stats as stats
    r_f = 0
    r_r = 0
    a_f = 0
    a_r = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        ref_variant = reffile.fetch(pileupcolumn.reference_name,pos,pos+1)
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos:
                if not pileupread.is_del and not pileupread.is_refskip and not pileupread.alignment.is_reverse:
                    if pileupread.alignment.query_sequence[pileupread.query_position] == ref_variant:
                        r_f += 1
                    if pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                        a_f += 1
                if not pileupread.is_del and not pileupread.is_refskip and pileupread.alignment.is_reverse:
                    if pileupread.alignment.query_sequence[pileupread.query_position] == ref_variant:
                        r_r += 1
                    if pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                        a_r += 1               
    oddsratio, pvalue = stats.fisher_exact([[r_f, r_r], [a_f, a_r]])
    return pvalue
    
def average_mismatches(samfile,reffile,chrom,pos,alt):
    
    ### Calculates average of the nM tag
    
    mismatches = 0
    count = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos and not pileupread.is_del and not pileupread.is_refskip:
                if pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                    count += 1
                    mismatches += pileupread.alignment.get_tag("nM")
    fin = float(mismatches)/float(count)
    return fin

def add_labels(chrom,pos,alt):
    
    ### Adds labels - error:0, SNP:1, unlabelled:2
    
    to_label = chrom + ":" + str(pos+1) + ":" + alt
    if "ERC" in to_label:
        return 0
    snps = "/users/ludwig/bgn494/sharedscratch/barretts_sc_project/bulk_bams_separately/ground_truth/GEN02021_ground_truth.txt"
    with open(snps) as f:
        if to_label in f.read():
            return 1
        else:
            return(2)