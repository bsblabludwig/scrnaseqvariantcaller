#!/usr/bin/env python

import numpy as np
import pandas as pd
import pysam
import sys
import unittest
from modules.preprocessing import *



class TestSum(unittest.TestCase):
    def test_find_variant_positions_output_type(self):
        """
        Test that output is a list.
        """
        samfile = pysam.AlignmentFile("test_data/test_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = find_variant_positions(samfile,reffile)
        self.assertIsInstance(result, list)
        
    def test_find_variants_output_type(self):
        """
        Test that output is a list.
        """
        samfile = pysam.AlignmentFile("test_data/test_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = find_variants(samfile,reffile,'chr10', 100529439)
        self.assertEqual(result, [])
        
    def test_find_variants_identifies_alternative_alleles(self):
        """
        Test that it finds correct variants alternative variants.
        """
        samfile = pysam.AlignmentFile("test_data/test_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = find_variants(samfile,reffile,'chr10', 100529438)[0][2]
        self.assertEqual(result, "T")
        
    def test_find_variants_correct_counts(self):
        """
        Test that it correctly counts the number of reads supporting the alternative allele.
        """
        samfile = pysam.AlignmentFile("test_data/test_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = find_variants(samfile,reffile,'chr10', 100529438)[0][3]
        self.assertEqual(result, 5)
        
    def test_find_variants_calls_multiple_variants(self):
        """
        Test that it correctly counts the number of reads supporting the alternative allele.
        """
        samfile = pysam.AlignmentFile("test_data/test2_bam.bam", "rb",reference_filename="/well/ludwig/users/hdp075/resources/MergedGenomes/Merged_GRCh38_ERCC_EBV_HP.fa")
        reffile = pysam.FastaFile("/well/ludwig/users/hdp075/resources/MergedGenomes/Merged_GRCh38_ERCC_EBV_HP.fa")
    
        result = len(find_variants(samfile,reffile,'chr10', 103755391))
        self.assertEqual(result, 2)
        
    def test_find_variants_doesnt_call_ref_variants(self):
        """
        Test that it correctly counts the number of reads supporting the alternative allele.
        """
        samfile = pysam.AlignmentFile("test_data/test2_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = len(find_variants(samfile,reffile,'chr10', 103755384))
        self.assertEqual(result, 0)
        
    def test_get_coverage(self):
        """
        Test that it correctly counts the number of reads covering a position.
        """
        samfile = pysam.AlignmentFile("test_data/test_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = get_coverage(samfile,reffile,'chr10', 100529438)
        self.assertEqual(result, 13)
        
    def test_get_average_mapping_quality(self):
        """
        Test that it correctly counts the average mapping quality of reads supporting variant.
        """
        samfile = pysam.AlignmentFile("test_data/test_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = get_average_mapping_quality(samfile,reffile,'chr10', 100529438,"T")
        self.assertEqual(result, 60)
        
    def test_get_average_base_quality(self):
        """
        Test that it correctly counts the average mapping quality of reads supporting variant.
        """
        samfile = pysam.AlignmentFile("test_data/test_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = get_average_base_quality(samfile,reffile,'chr10', 100529438,"T")
        self.assertEqual(result, 40.0)
        
    def test_find_variants_without_duplicates(self):
        """
        Test that it correctly counts the average mapping quality of reads supporting variant.
        """
        samfile = pysam.AlignmentFile("test_data/test3_markdup.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = find_variants_without_duplicates(samfile,reffile,"ERCC-00002",325,"A")
        self.assertEqual(result, [7, 0.09])
        
    def test_fisher_exact_test(self):
        """
        Test that it correctly counts the average mapping quality of reads supporting variant.
        """
        samfile = pysam.AlignmentFile("test_data/test_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = fisher_exact_test(samfile,reffile,'chr10', 100529438,"T")
        self.assertEqual(result, 1.0)
        
    def test_strand_test_present_on_one_strand_only(self):
        """
        Test that it correctly counts the average mapping quality of reads supporting variant.
        """
        samfile = pysam.AlignmentFile("test_data/test_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = strand_test(samfile,reffile,'chr10', 100529407,"T")
        self.assertEqual(result, 0)
        
    def test_strand_test_present_on_both_strands(self):
        """
        Test that it correctly counts the average mapping quality of reads supporting variant.
        """
        samfile = pysam.AlignmentFile("test_data/test_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = strand_test(samfile,reffile,'chr10', 100529438,"T")
        self.assertEqual(result, 1)
        
    def test_average_mismatches(self):
        """
        Test that it correctly counts the average mapping quality of reads supporting variant.
        """
        samfile = pysam.AlignmentFile("test_data/test_bam.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = average_mismatches(samfile,reffile,'chr10', 100529438,"T")
        self.assertEqual(result, 2.0)
     
    def test_add_labels_snp(self):
        """
        Test that it correctly identifies an SNP.
        """
        samfile = pysam.AlignmentFile("test_data/GEN02021_Barret_Manual_P4_A1_markdup.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = add_labels("chr10",5107510,"A")
        self.assertEqual(result, 1)
    
    def test_add_labels_ercc(self):
        """
        Test that it correctly identifies an error from ERCC.
        """
        samfile = pysam.AlignmentFile("test_data/GEN02021_Barret_Manual_P4_A1_markdup.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("test_data/test_ref.fa")
    
        result = add_labels("ERCC-00002",105,"A")
        self.assertEqual(result, 0)
        
    def test_add_labels_unlabelled(self):
        """
        Test that it correctly identifies an error from ERCC.
        """
        samfile = pysam.AlignmentFile("test_data/GEN02021_Barret_Manual_P4_A1_markdup.bam", "rb",reference_filename="/well/ludwig/users/hdp075/resources/MergedGenomes/Merged_GRCh38_ERCC_EBV_HP.fa")
        reffile = pysam.FastaFile("/well/ludwig/users/hdp075/resources/MergedGenomes/Merged_GRCh38_ERCC_EBV_HP.fa")
    
        result = add_labels("chr10",100364642,"T")
        self.assertEqual(result, 2)


if __name__ == '__main__':
    unittest.main()