#!/usr/bin/env python

import numpy as np
import pandas as pd
import pysam
import sys
import csv
from scripts.get_parameters_modules import *
import click

@click.command()
@click.option('bamfile', '--input-file', '-i', required=True, help="Input BAM file")
@click.option('reference', '--reference', '-f', required=True,help="Reference genome (fasta with fai index)")
@click.option('outfile', '--out', '-o', required=True, help="Name of output file")

def calculate_parameters(bamfile,reference,outfile):

    ### Performs a pileup over the input BAM, calculates variant parameters
    
    samfile = pysam.AlignmentFile(bamfile, "rb",reference_filename=reference)
    reffile = pysam.FastaFile(reference)
    
    outname = outfile + ".txt"
    params_out = open(outname, 'a+')
    data_line = csv.writer(params_out, delimiter='\t', lineterminator='\n')
    data_line.writerow(["CHROM","POS","ALT","DP","AD_ALT","AF","AVG_MAPQ","AVG_BAQ","FS","AVG_nM_distance","NO_PCR_GROUPS","FR_PCR_GROUPS","LABEL"])

    for position in find_variant_positions(samfile,reffile):
        variants_list = find_variants(samfile,reffile,position[0], position[1])
        for specific_variant_info in variants_list:
            CHROM = specific_variant_info[0]
            POS = specific_variant_info[1]
            ALT = specific_variant_info[2]
            AD_ALT = specific_variant_info[3]
            if AD_ALT >=3: 
                DP = get_coverage(samfile,reffile,specific_variant_info[0], specific_variant_info[1])
                AF = round(float(specific_variant_info[3])/float(DP),2)
                if AF >= 0.01:
                    FR_PCR_GROUPS = find_variants_without_duplicates(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])[1]
                    if FR_PCR_GROUPS >= 0.01:
                        AVG_MAPQ = get_average_mapping_quality(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])
                        AVG_BAQ = get_average_base_quality(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2]) 
                        FS = fisher_exact_test(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])      
                        AVG_nM_distance = average_mismatches(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2]) 
                        NO_PCR_GROUPS = find_variants_without_duplicates(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])[0] 
                        LABEL = add_labels(specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])
                        data_line.writerow([CHROM,POS,ALT,DP,AD_ALT,AF,AVG_MAPQ,AVG_BAQ,FS,AVG_nM_distance,NO_PCR_GROUPS,FR_PCR_GROUPS,LABEL])
 
if __name__ == '__main__':
    calculate_parameters()