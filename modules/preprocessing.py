#!/usr/bin/env python

import numpy as np
import pandas as pd
import pysam
import sys
import csv
import click

@click.command()
@click.option('bamfile', '--input-file', '-i', required=True, help="Input BAM file")
@click.option('known_snps', '--snp-list', '-s', required=True, help="List of known SNPs")
@click.option('reference', '--reference', '-f', required=True,help="Reference genome (fasta with fai index)")
@click.option('outfile', '--out', '-o', required=True, help="Name of output file")

def calculate_parameters(bamfile,reference,outfile,known_snps):

    ### Performs a pileup over the input BAM, calculates variant parameters
    
    samfile = pysam.AlignmentFile(bamfile, "rb",reference_filename=reference)
    reffile = pysam.FastaFile(reference)
    
    outname = outfile + ".txt"
    params_out = open(outname, 'a+')
    data_line = csv.writer(params_out, delimiter='\t', lineterminator='\n')
    data_line.writerow(["CHROM","POS","ALT","DP","AD_ALT","AF","AVG_MAPQ","AVG_BAQ","FS","AVG_nM_distance","NO_PCR_GROUPS","FR_PCR_GROUPS","LABEL"])

    for position in find_variant_positions(samfile,reffile):
        variants_list = find_variants(samfile,reffile,position[0], position[1])
        for specific_variant_info in variants_list:
            CHROM = specific_variant_info[0]
            POS = specific_variant_info[1]
            ALT = specific_variant_info[2]
            AD_ALT = specific_variant_info[3]
            if AD_ALT >=3: 
                DP = get_coverage(samfile,reffile,specific_variant_info[0], specific_variant_info[1])
                AF = round(float(specific_variant_info[3])/float(DP),2)
                STRAND = strand_test(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])
                if AF >= 0.01 and STRAND == 1:
                    FR_PCR_GROUPS = find_variants_without_duplicates(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])[1]
                    if FR_PCR_GROUPS >= 0.01:
                        AVG_MAPQ = get_average_mapping_quality(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])
                        AVG_BAQ = get_average_base_quality(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2]) 
                        if AVG_BAQ >= 20:
                            FS = fisher_exact_test(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])
                            AVG_nM_distance = average_mismatches(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2]) 
                            NO_PCR_GROUPS = find_variants_without_duplicates(samfile,reffile,specific_variant_info[0],specific_variant_info[1],specific_variant_info[2])[0] 
                            LABEL = add_labels(specific_variant_info[0],specific_variant_info[1],specific_variant_info[2],known_snps)
                            data_line.writerow([CHROM,POS,ALT,DP,AD_ALT,AF,AVG_MAPQ,AVG_BAQ,FS,AVG_nM_distance,NO_PCR_GROUPS,FR_PCR_GROUPS,LABEL])
 

def find_variant_positions(samfile,reffile):
    
    ### Iterates through the alignment and identifies positions with alternative alleles
    ### Output: list of list : [[chrom,pos],[chrom,pos]...]
    
    pileupout = []
    for pileupcolumn in samfile.pileup():
        if pileupcolumn.n >= 3:
            variants = []
            ref_variant = reffile.fetch(pileupcolumn.reference_name,pileupcolumn.pos,pileupcolumn.pos+1)
            a = ref_variant
            for pileupread in pileupcolumn.pileups:
                if a == ref_variant:
                    if not pileupread.is_del and not pileupread.is_refskip:
                        a = pileupread.alignment.query_sequence[pileupread.query_position]
                if a != ref_variant:
                    if "chr" in pileupcolumn.reference_name or "ERC" in pileupcolumn.reference_name:
                        pileupout.append([pileupcolumn.reference_name,pileupcolumn.pos])
                        break                
    return pileupout

    
def find_variants(samfile,reffile,chrom,pos):
    
    ### Finds variant alleles for given genomic position and counts reads supporting them
    ### Output: list of lists [chrom,pos,alt,count] - sometimes multiple variants
    
    variants = []
    output = []
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        ref_variant = reffile.fetch(pileupcolumn.reference_name,pos,pos+1)
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos:
                if not pileupread.is_del and not pileupread.is_refskip:
                    if pileupread.alignment.query_sequence[pileupread.query_position] != ref_variant:
                        variants.append(pileupread.alignment.query_sequence[pileupread.query_position])
    a = variants.count("A")
    c = variants.count("C")
    g = variants.count("G")
    t = variants.count("T")
    if "A" != ref_variant and a != 0:
        output.append([chrom,pos,"A",a])
    if "C" != ref_variant and c != 0:
        output.append([chrom,pos,"C",c])
    if "G" != ref_variant and g != 0:
        output.append([chrom,pos,"G",g])
    if "T" != ref_variant and t != 0:
        output.append([chrom,pos,"T",t])
                
    return output    

def get_coverage(samfile,reffile,chrom,pos): 
    
    ### Finds number of reads covering the region
    
    coverage = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos and not pileupread.is_del and not pileupread.is_refskip:
                coverage += 1
    return coverage    
        #if pileupcolumn.pos == pos:
        #    return pileupcolumn.n  

def get_average_mapping_quality(samfile,reffile,chrom,pos,alt): 
    
    ### Finds average mapping quality of reads supporting the variant
    
    mapq = 0
    count = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos and not pileupread.is_del and not pileupread.is_refskip and pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                mapq += pileupread.alignment.mapping_quality
                count += 1
    return float(mapq)/float(count)
  
def get_average_base_quality(samfile,reffile,chrom,pos,alt): ##IN PROGRESS##
    
    ### Finds average base quality of alternative bases
    
    baq = 0
    count = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos and not pileupread.is_del and not pileupread.is_refskip and pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                baq += pileupread.alignment.query_qualities[pileupread.query_position]
                count += 1
    return float(baq)/float(count)
    
    
def find_variants_without_duplicates(samfile,reffile,chrom,pos,alt):
    
    ### Finds variant alleles for given genomic position and counts reads supporting them - PCR duplicates removed
    ### Output: list with [number of PCR groups present in, % of PCR groups present in (AF with PCR groups removed)]
    
    supporting_reads = 0
    all_reads = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos:
                if not pileupread.is_del and not pileupread.is_refskip and not pileupread.alignment.is_duplicate:
                    all_reads += 1
                    if pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                        supporting_reads += 1 
    af = round(float(supporting_reads)/float(all_reads),2)       
    return [supporting_reads, af]
    
    
def fisher_exact_test(samfile,reffile,chrom,pos,alt):
    
    ### Calculates a p-value for the Fisher Exact Test
    ### Input: [[ref_forward,ref_reverse],[alt_forward,alt_reverse]]
    
    import scipy.stats as stats
    r_f = 0
    r_r = 0
    a_f = 0
    a_r = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        ref_variant = reffile.fetch(pileupcolumn.reference_name,pos,pos+1)
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos:
                if not pileupread.is_del and not pileupread.is_refskip and not pileupread.alignment.is_reverse:
                    if pileupread.alignment.query_sequence[pileupread.query_position] == ref_variant:
                        r_f += 1
                    if pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                        a_f += 1
                if not pileupread.is_del and not pileupread.is_refskip and pileupread.alignment.is_reverse:
                    if pileupread.alignment.query_sequence[pileupread.query_position] == ref_variant:
                        r_r += 1
                    if pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                        a_r += 1               
    oddsratio, pvalue = stats.fisher_exact([[r_f, r_r], [a_f, a_r]])
    return pvalue
 
def strand_test(samfile,reffile,chrom,pos,alt):
    
    ### Tests that a variant is present on both strands
    
    a_f = 0
    a_r = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        ref_variant = reffile.fetch(pileupcolumn.reference_name,pos,pos+1)
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos:
                if not pileupread.is_del and not pileupread.is_refskip and not pileupread.alignment.is_reverse:
                    if pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                        a_f += 1
                if not pileupread.is_del and not pileupread.is_refskip and pileupread.alignment.is_reverse:
                    if pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                        a_r += 1               
    if a_f == 0 or a_r == 0:
        return 0
    else:
        return 1

 
def average_mismatches(samfile,reffile,chrom,pos,alt):
    
    ### Calculates average of the nM tag
    
    mismatches = 0
    count = 0
    for pileupcolumn in samfile.pileup(chrom, pos, pos+1):  
        for pileupread in pileupcolumn.pileups:
            if pileupcolumn.pos == pos and not pileupread.is_del and not pileupread.is_refskip:
                if pileupread.alignment.query_sequence[pileupread.query_position] == alt:
                    count += 1
                    try:
                        mismatches += pileupread.alignment.get_tag("nM")
                    except:
                        mismatches += pileupread.alignment.get_tag("NM")
    fin = float(mismatches)/float(count)
    return fin

def add_labels(chrom,pos,alt,snp_list):
    
    ### Adds labels - error:0, SNP:1, unlabelled:2
    
    to_label = chrom + ":" + str(pos+1) + ":" + alt
    if "ERC" in to_label:
        return 0
    with open(snp_list) as f:
        if to_label in f.read():
            return 1
        else:
            return 2


if __name__ == '__main__':
    calculate_parameters()