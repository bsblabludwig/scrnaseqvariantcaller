#!/usr/bin/env python

import numpy as np
import pandas as pd
import pysam
import sys
import csv
import click
import statistics

@click.command()
@click.option('variants_file', '--variants-file', '-v', required=True, help="Name of the file with variants after reverse transcriptase filtering")
@click.option('outfile', '--out', '-o', required=True, help="Name of output file")

def final_filtering(variants_file,outfile):

    ### Apply final filtering
    
    varfile = pd.read_csv(variants_file, sep="\t")
    
    rna_editing = remove_rna_editing_sites(varfile)
    rna_editing["POS[0-based]"] = rna_editing["POS[0-based]"].astype(int)
    rna_editing["POS[1-based]"] = rna_editing["POS[1-based]"].astype(int)
    post_filtering = rna_editing.values.tolist()
    outname = outfile + ".bed"
    calls_out = open(outname, 'a+')
    data_line = csv.writer(calls_out, delimiter='\t', lineterminator='\n')
    data_line.writerow(["CHROM","POS[0-based]","POS[1-based]","ALT","AF"])
    for each in post_filtering:
        data_line.writerow([each[0],each[1],each[2],each[3],each[4]])

def remove_rna_editing_sites(varfile):
    
    ### Removes RNA editing sites from REDi and DARNED databases
    
    varfile["POS[0-based]"] = varfile["POS[0-based]"].astype(int)
    redi = pd.read_csv("data/rna_editing_hg38_REDi.bed", sep="\t", usecols=[0,1])
    redi.columns = ["CHROM","POS[0-based]"]
    darned = pd.read_csv("data/rna_editing_hg38_DARNED.bed", sep="\t", usecols=[0,1])
    darned.columns = ["CHROM","POS[0-based]"]
    
    varfile = varfile.merge(redi,on = ["CHROM","POS[0-based]"], how="outer",indicator = True, left_index=False, right_index=False).query('_merge=="left_only"').drop("_merge",axis=1)
    out_varfile = varfile.merge(darned,on = ["CHROM","POS[0-based]"], how="outer",indicator = True, left_index=False, right_index=False).query('_merge=="left_only"').drop("_merge",axis=1)

    return out_varfile
  
if __name__ == '__main__':
    final_filtering()