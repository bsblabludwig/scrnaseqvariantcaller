#!/usr/bin/env python

import numpy as np
import pandas as pd
import pysam
import sys
import csv
import click
import statistics
import re

@click.command()
@click.option('bamfile', '--input-file', '-i', required=True, help="Input BAM file")
@click.option('reference', '--reference', '-f', required=True,help="Reference genome (fasta with fai index)")
@click.option('variants_file', '--variants-file', '-v', required=True, help="Name of the preprocessed variant file")
@click.option('outfile', '--out', '-o', required=True, help="Name of output file")
@click.option('known_snps', '--snp-list', '-s', required=True, help="List of known SNPs")

def remove_reverse_transcriptase_errors(bamfile,reference,variants_file,outfile):

    ### Removes reverse transcriptase errors
    
    samfile = pysam.AlignmentFile(bamfile, "rb",reference_filename=reference)
    reffile = pysam.FastaFile(reference)
    varfile = pd.read_csv(variants_file, sep="\t", usecols=["CHROM","POS","ALT","AF"])
    varfile = varfile.loc[varfile["AF"] >= 0.1]
    
    pairs = make_variant_pairs(varfile)

    filtered_variants_tmp,to_remove = filter_pairs(samfile,reffile,pairs)
    filtered_variants = []
    for each in filtered_variants_tmp:
        if each not in to_remove and each not in filtered_variants:
            filtered_variants.append(each)
    final_calls = []
    for each in filtered_variants:
        alt_call = compare_within_transcripts(each, varfile)
        if alt_call != None and alt_call not in final_calls:
            final_calls.append(alt_call[0] + [alt_call[1]])
        if alt_call != None:
            calls_list = find_min_var_AF_within_transcripts(alt_call[0], varfile)
            if calls_list != None:
                for a in calls_list:
                    if a != None and a not in final_calls:
                        final_calls.append(a)
                        
    varfile = pd.read_csv(variants_file, sep="\t", usecols=["CHROM","POS","ALT","AF","DP"])
    good_support_variants = varfile.loc[varfile["AF"] >= 0.4].loc[varfile["DP"] >= 50]
    good_support_variants_chrom = list(good_support_variants["CHROM"])
    good_support_variants_pos = list(good_support_variants["POS"])
    good_support_variants_alt = list(good_support_variants["ALT"])
    good_support_variants_af = list(good_support_variants["AF"])
    for i in range(len(good_support_variants_chrom)):
        good_support_var = [good_support_variants_chrom[i],good_support_variants_pos[i],good_support_variants_alt[i],good_support_variants_af[i]]
        if good_support_var not in final_calls:
            final_calls.append(good_support_var)
    outname = outfile + ".bed"
    calls_out = open(outname, 'a+')
    data_line = csv.writer(calls_out, delimiter='\t', lineterminator='\n')
    data_line.writerow(["CHROM","POS[0-based]","POS[1-based]","ALT","AF"])
    for each in final_calls:
        data_line.writerow([each[0],each[1],each[1]+1,each[2],each[3]])
    
def make_variant_pairs(varfile):
    
    ### Iterates through the list of variants from the preprocessed file 
    ### and outputs pairs of variants occurring at close proximity
    
    pairs = []
    chromosomes = list(varfile.iloc[:,0])
    positions = list(varfile.iloc[:,1])
    alts = list(varfile.iloc[:,2])
    for i in range(len(chromosomes)-1):
        if chromosomes[i] == chromosomes[i+1]:
            if int(positions[i+1])-int(positions[i]) <= 200:
                pairs.append([chromosomes[i], positions[i], alts[i], chromosomes[i+1], positions[i+1], alts[i+1]])        
        try:
            if chromosomes[i] == chromosomes[i+2]:
                if int(positions[i+2])-int(positions[i]) <= 200:
                    pairs.append([chromosomes[i], positions[i], alts[i], chromosomes[i+2], positions[i+2], alts[i+2]])        
        except:
            pass
    return pairs

def filter_pairs(samfile,reffile,pairs):
    
    ### Filters out RT errors based on comparison of variants in a pair
    
    filt_vars = []
    to_remove = []
    
    for i in range(len(pairs)):
        dif = pairs[i][4] - pairs[i][1]
        first = 0
        second = 0
        both = 0
        odd_count = 0
        for pileupcolumn in samfile.pileup(pairs[i][0], pairs[i][1], pairs[i][1]+1, min_base_quality=1, redo_baq=True): 
            for pileupread in pileupcolumn.pileups:
                if pileupcolumn.pos == pairs[i][1]:
                    if not pileupread.is_del and not pileupread.is_refskip:
                        try: 
                            current_cigar = pileupread.alignment.cigarstring 
                            softclips = re.findall(r"(\d+)S", current_cigar)
                            if len(softclips) == 1:
                                if current_cigar[-1] == "S":
                                    start_clip = 0
                                    end_clip = int(softclips[0])
                                else:
                                    end_clip = 0
                                    start_clip = int(softclips[0])
                            if len(softclips) == 2:
                                start_clip = int(softclips[0])
                                end_clip = int(softclips[1])
                            if pileupread.query_position >= start_clip-1 and pileupread.query_position+dif <= len(pileupread.alignment.query_sequence)-end_clip:
                                if pileupread.alignment.query_sequence[pileupread.query_position] == pairs[i][2] and pileupread.alignment.query_sequence[pileupread.query_position+dif] == pairs[i][5]:
                                    both += 1                                 
                                if pileupread.alignment.query_sequence[pileupread.query_position] == pairs[i][2] and pileupread.alignment.query_sequence[pileupread.query_position+dif] != pairs[i][5]:
                                    first += 1   
                                if pileupread.alignment.query_sequence[pileupread.query_position] != pairs[i][2] and pileupread.alignment.query_sequence[pileupread.query_position+dif] == pairs[i][5]:
                                    second += 1                                 
                        except:
                            pass
        if both > 0:
            if first > 0 or second > 0:
                    if first == 1:
                        for pileupcolumn in samfile.pileup(pairs[i][0], pairs[i][1], pairs[i][1]+1, min_base_quality=1,redo_baq=True): 
                            for pileupread in pileupcolumn.pileups:
                                if pileupcolumn.pos == pairs[i][1]:
                                    if not pileupread.is_del and not pileupread.is_refskip:
                                        try: 
                                            if pileupread.alignment.query_sequence[pileupread.query_position] == pairs[i][2] and pileupread.alignment.query_sequence[pileupread.query_position+dif] != pairs[i][5]: # and pileupread.alignment.query_qualities[pileupread.query_position] >= 20 and pileupread.alignment.query_qualities[pileupread.query_position+dif] >= 20:
                                                odd_variant = pileupread.alignment.query_sequence[pileupread.query_position+dif]
                                                for pileupcolumn in samfile.pileup(pairs[i][3], pairs[i][4], pairs[i][4]+1, min_base_quality=1,redo_baq=True): 
                                                    for pileupread in pileupcolumn.pileups:
                                                        if pileupcolumn.pos == pairs[i][4]:
                                                            if not pileupread.is_del and not pileupread.is_refskip:
                                                                try: 
                                                                    if pileupread.alignment.query_sequence[pileupread.query_position+dif] == odd_variant and pileupread.alignment.query_qualities[pileupread.query_position+dif] >= 20:
                                                                        odd_count += 1
                                                                except:
                                                                    pass
                                        except:
                                            pass
                        if odd_count > 1:
                            filt_vars.append([pairs[i][0],pairs[i][1],pairs[i][2]]) 
                        else:
                            to_remove.append([pairs[i][0],pairs[i][1],pairs[i][2]])
                                            
                        
                    if second == 1:
                        for pileupcolumn in samfile.pileup(pairs[i][0], pairs[i][1], pairs[i][1]+1, min_base_quality=1,redo_baq=True): 
                            for pileupread in pileupcolumn.pileups:
                                if pileupcolumn.pos == pairs[i][1]:
                                    if not pileupread.is_del and not pileupread.is_refskip:
                                        try: 
                                            if pileupread.alignment.query_sequence[pileupread.query_position] != pairs[i][2] and pileupread.alignment.query_sequence[pileupread.query_position+dif] == pairs[i][5]: # and pileupread.alignment.query_qualities[pileupread.query_position] >= 20 and pileupread.alignment.query_qualities[pileupread.query_position+dif] >= 20:
                                                odd_variant = pileupread.alignment.query_sequence[pileupread.query_position]
                                                for pileupcolumn in samfile.pileup(pairs[i][0], pairs[i][1], pairs[i][1]+1, min_base_quality=1,redo_baq=True): 
                                                    for pileupread in pileupcolumn.pileups:
                                                        if pileupcolumn.pos == pairs[i][1]:
                                                            if not pileupread.is_del and not pileupread.is_refskip:
                                                                try: 
                                                                    if pileupread.alignment.query_sequence[pileupread.query_position] == odd_variant and pileupread.alignment.query_qualities[pileupread.query_position] >= 20:
                                                                        odd_count += 1
                                                                except:
                                                                    pass
                                        except:
                                            pass
                        if odd_count > 1:
                            filt_vars.append([pairs[i][3],pairs[i][4],pairs[i][5]])                           
                        else:
                            to_remove.append([pairs[i][3],pairs[i][4],pairs[i][5]])
                                
                    if first > 1:
                        filt_vars.append([pairs[i][0],pairs[i][1],pairs[i][2]]) 
                    if second > 1:    
                        filt_vars.append([pairs[i][3],pairs[i][4],pairs[i][5]])     
                    if first == 0:
                            to_remove.append([pairs[i][0],pairs[i][1],pairs[i][2]])  
                    if second == 0:
                            to_remove.append([pairs[i][3],pairs[i][4],pairs[i][5]])
    return filt_vars, to_remove  


def compare_within_transcripts(filtered_variant, varfile):

    ### Compares AF to other variants in the transcript region

    transcript_pd = pd.read_csv("data/gencode_transcript_regs_hg38.bed", sep="\t", header=None)
    try:
        transcript = transcript_pd.loc[transcript_pd.iloc[:,0] == filtered_variant[0]]
        transcript = transcript.loc[transcript.iloc[:,1] <= filtered_variant[1]]
        transcript = transcript.loc[transcript.iloc[:,2] >= filtered_variant[1]]
        low_lim = min(list(transcript.iloc[:,1]))
        up_lim = max(list(transcript.iloc[:,2]))
        varfile = varfile.loc[varfile.iloc[:,0] == filtered_variant[0]]
        varfile = varfile.loc[varfile.iloc[:,1] >= low_lim]
        varfile = varfile.loc[varfile.iloc[:,1] <= up_lim]
        af_var = float(varfile.loc[varfile.iloc[:,1] == filtered_variant[1]].iloc[:,3])
        varfile = varfile.loc[varfile.iloc[:,1] != filtered_variant[1]]
        af_mean = statistics.mean([float(a) for a in list(varfile.iloc[:,3])])
        if af_var > af_mean:
            return filtered_variant, af_var, af_mean
    except:
        pass
        
def find_min_var_AF_within_transcripts(filtered_variant, varfile):

    ### Compares all known variants (from step before and called from scs), finds the lowest AF and finally calls all positions from the reference above this threshold

    transcript_pd = pd.read_csv("data/gencode_transcript_regs_hg38.bed", sep="\t", header=None)
    try:
        transcript = transcript_pd.loc[transcript_pd.iloc[:,0] == filtered_variant[0]]
        transcript = transcript.loc[transcript.iloc[:,1] <= filtered_variant[1]]
        transcript = transcript.loc[transcript.iloc[:,2] >= filtered_variant[1]]
        known_SNPs = pd.read_csv(known_snps, sep=":", header=None)
        known_SNPs.columns = ["CHROM","POS","ALT"]
        known_SNPs_AF = pd.merge(varfile, known_SNPs)
        low_lim = min(list(transcript.iloc[:,1]))
        up_lim = max(list(transcript.iloc[:,2]))
        varfile = varfile.loc[varfile.iloc[:,0] == filtered_variant[0]]
        known_SNPs_AF = known_SNPs_AF.loc[known_SNPs_AF.iloc[:,0] == filtered_variant[0]]
        known_SNPs_AF = known_SNPs_AF.loc[known_SNPs_AF.iloc[:,1] <= up_lim]
        known_SNPs_AF = known_SNPs_AF.loc[known_SNPs_AF.iloc[:,1] >= low_lim]
        varfile = varfile.loc[varfile.iloc[:,1] >= low_lim]
        varfile = varfile.loc[varfile.iloc[:,1] <= up_lim]
        af_var = float(varfile.loc[varfile.iloc[:,1] == filtered_variant[1]].iloc[:,3])
        af_threshold = min(min(list(known_SNPs_AF.iloc[:,3])), af_var)
        calls = varfile.loc[varfile.iloc[:,3] >= af_threshold]       
        return calls.values.tolist()
    
    except:
        pass


if __name__ == '__main__':
    remove_reverse_transcriptase_errors()