#!/usr/bin/env python

import numpy as np
import pandas as pd
import pysam
import sys
import unittest
from modules.remove_rt_errors import *



class TestSum(unittest.TestCase):
    def test_make_variant_pairs_remove(self):
        """
        Test that correctly identifies variant pairs.
        """
        
        varfile = pd.DataFrame(columns=["CHROM","POS","ALT"])
        varfile.iloc[:,0] = ["chr1","chr1"]
        varfile.iloc[:,1] = [100,400]
        varfile.iloc[:,2] = ["C","A"]
        result = make_variant_pairs(varfile)
        self.assertEqual(result, [])
        
    def test_make_variant_pairs_call(self):
        """
        Test that correctly identifies variant pairs.
        """
        
        varfile = pd.DataFrame(columns=["CHROM","POS","ALT"])
        varfile.iloc[:,0] = ["chr1","chr1"]
        varfile.iloc[:,1] = [100,150]
        varfile.iloc[:,2] = ["C","A"]
        result = make_variant_pairs(varfile)
        self.assertEqual(len(result[0]), 6)
        
    def test_filter_pairs(self):
        """
        Test that correctly classifies a variant as potential real mutation or RT error based on comparison of reads.
        """
        samfile = pysam.AlignmentFile("tests/test_data/GEN02021_Barret_Manual_P4_D8_markdup.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("tests/test_data/test_ref.fa")
        
        result = filter_pairs(samfile,reffile,[["chr12",53479758,"G","chr12",53479759,"G"]])
        self.assertEqual(result,([], [['chr12', 53479759, 'G'], ['chr12', 53479758, 'G']]))
        
    def test_filter_pairs_doesnt_call_if_one_support_and_low_quality(self):
        """
        Test that doesn't call a variant if it has only 1 supporting read (which doesn't contain the other variant) and the base quality is < 20.
        """
        samfile = pysam.AlignmentFile("tests/test_data/GEN02021_Barret_Manual_P4_A1_markdup.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("tests/test_data/test_ref.fa")
        
        result = filter_pairs(samfile,reffile,[['chr15', 40038316, 'G','chr15', 40038318, 'G']])
        self.assertEqual(result,([['chr15', 40038318, 'G'],['chr15', 40038316, 'G']],[]))
        
    def test_filter_pairs_doesnt_call_if_differ_by_seq_error(self):
        """
        Test that doesn't call a variant if there is a seq error instead of the other variant.
        """
        samfile = pysam.AlignmentFile("tests/test_data/GEN02021_Barret_Manual_P4_A1_markdup.bam", "rb",reference_filename="test_data/test_ref.fa")
        reffile = pysam.FastaFile("tests/test_data/test_ref.fa")
        
        result = filter_pairs(samfile,reffile,[['chr2', 32866888, 'T','chr2', 32866939, 'T']])
        self.assertEqual(result,([['chr2', 32866888, 'T']], [['chr2', 32866939, 'T']]))        
        
    def test_compare_within_transcripts_calls_if_higher_AF(self):
        """
        Test that it calls SNP if AF is higher than the mean in the transcript.
        """
        varfile = pd.DataFrame(columns=["CHROM","POS","ALT","AF"])
        varfile.iloc[:,0] = ["chr10","chr10","chr10"]
        varfile.iloc[:,1] = [71816549,71816550,71816551]
        varfile.iloc[:,2] = ["C","A","T"]
        varfile.iloc[:,3] = [0.5,0.1,0.1]
        
        result = compare_within_transcripts(['chr10', 71816549, 'T'], varfile)
        self.assertEqual(result,(['chr10', 71816549, 'T'], 0.5, 0.1))
        
        
    def test_compare_within_transcripts_calls_if_doesnt_call_low_AF(self):
        """
        Test that it doesn't call a variant if AF is lower than the mean in the transcript.
        """
        
        varfile = pd.DataFrame(columns=["CHROM","POS","ALT","AF"])
        varfile.iloc[:,0] = ["chr10","chr10","chr10"]
        varfile.iloc[:,1] = [71816549,71816550,71816551]
        varfile.iloc[:,2] = ["C","A","T"]
        varfile.iloc[:,3] = [0.01,0.1,0.1]
        
        result = compare_within_transcripts(['chr10', 71816549, 'T'], varfile)
        self.assertIsNone(result)
        
    def test_find_min_var_AF_within_transcripts(self):
        """
        Test that it doesn't call a variant if AF=1 and other alternative alleles in transcript have AF=1.
        """
        
        varfile = pd.DataFrame(columns=["CHROM","POS","ALT","AF"])
        varfile.iloc[:,0] = ["chr2","chr2","chr2","chr2"]
        varfile.iloc[:,1] = [32866783,32866782,32866784,32866785]
        varfile.iloc[:,2] = ["T","A","T","T"]
        varfile.iloc[:,3] = [1.0,0.6,0.8,0.9]
        
        result = find_min_var_AF_within_transcripts(['chr2', 32866783, 'T'], varfile)
        print(result)
        self.assertEqual(len(result),3)

if __name__ == '__main__':
    unittest.main()