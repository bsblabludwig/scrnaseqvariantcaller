#!/bin/bash

# Specify a job name
#$ -N run_scrnaseqcaller

# Project name and target queue
#$ -P group.prjc
#$ -q test.qc

# Run the job in the current working directory
#$ -cwd -j y

# Log locations which are relative to the current
# working directory of the submission
###$ -o output.log
###$ -e error.log

# Parallel environemnt settings
#  For more information on these please see the wiki
#  Allowed settings:
#   shmem
#   mpi
#   node_mpi
#   ramdisk
#$ -pe shmem 1

# Some useful data about the job to help with debugging
echo "------------------------------------------------"
echo "SGE Job ID: $JOB_ID"
echo "SGE Task ID: $SGE_TASK_ID"
echo "Run on host: "`hostname`
echo "Operating system: "`uname -s`
echo "Username: "`whoami`
echo "Started at: "`date`
echo "------------------------------------------------"

# Begin writing your script here

export PYTHONPATH=$PYTHONPATH:~/sharedscratch/barretts_sc_project/variant_calling_with_pysam/scrnaseqcaller/

#./scripts/run.py calculate-parameters -i /users/ludwig/bgn494/sharedscratch/barretts_sc_project/bams_uniquely_mapped_STAR/${1}_markdup.bam -f /well/ludwig/users/hdp075/resources/MergedGenomes/Merged_GRCh38_ERCC_EBV_HP.fa -s /users/ludwig/bgn494/sharedscratch/barretts_sc_project/bulk_bams_separately/ground_truth/all_haplotypecaller_calls_per_patient/GEN02025_all_SNPs.txt -o /users/ludwig/bgn494/sharedscratch/barretts_sc_project/variant_calling_with_pysam/scrnaseqcaller_output/output_files/${1}_calls

./scripts/run.py remove-reverse-transcriptase-errors -i /users/ludwig/bgn494/sharedscratch/barretts_sc_project/bams_uniquely_mapped_STAR/${1}_markdup.bam -v /users/ludwig/bgn494/sharedscratch/barretts_sc_project/variant_calling_with_pysam/scrnaseqcaller_output/output_files/${1}_calls.txt -f /well/ludwig/users/hdp075/resources/MergedGenomes/Merged_GRCh38_ERCC_EBV_HP.fa -s /users/ludwig/bgn494/sharedscratch/barretts_sc_project/bulk_bams_separately/ground_truth/all_haplotypecaller_calls_per_patient/GEN02021_all_SNPs.txt -o /users/ludwig/bgn494/sharedscratch/barretts_sc_project/variant_calling_with_pysam/scrnaseqcaller_output/output_files/${1}_filtered 

./scripts/run.py final-filtering -v /users/ludwig/bgn494/sharedscratch/barretts_sc_project/variant_calling_with_pysam/scrnaseqcaller_output/output_files/${1}_filtered.bed -o /users/ludwig/bgn494/sharedscratch/barretts_sc_project/variant_calling_with_pysam/scrnaseqcaller_output/output_files/${1}_final 


#./scripts/run.py calculate-parameters -i /users/ludwig/bgn494/sharedscratch/barretts_sc_project/bams_uniquely_mapped_Hisat2/${1}_markdup.bam -f /well/ludwig/users/hdp075/resources/MergedGenomes/Merged_GRCh38_ERCC_EBV_HP.fa -s /users/ludwig/bgn494/sharedscratch/barretts_sc_project/bulk_bams_separately/ground_truth/all_haplotypecaller_calls_per_patient/GEN02025_all_SNPs.txt -o /users/ludwig/bgn494/sharedscratch/barretts_sc_project/variant_calling_with_pysam/scrnaseqcaller_output/output_files/${1}_Hisat2_calls

./scripts/run.py remove-reverse-transcriptase-errors -i /users/ludwig/bgn494/sharedscratch/barretts_sc_project/bams_uniquely_mapped_Hisat2/${1}_markdup.bam -v /users/ludwig/bgn494/sharedscratch/barretts_sc_project/variant_calling_with_pysam/scrnaseqcaller_output/output_files/${1}_Hisat2_calls.txt -f /well/ludwig/users/hdp075/resources/MergedGenomes/Merged_GRCh38_ERCC_EBV_HP.fa -s /users/ludwig/bgn494/sharedscratch/barretts_sc_project/bulk_bams_separately/ground_truth/all_haplotypecaller_calls_per_patient/GEN02021_all_SNPs.txt -o /users/ludwig/bgn494/sharedscratch/barretts_sc_project/variant_calling_with_pysam/scrnaseqcaller_output/output_files/${1}_Hisat2_filtered 

./scripts/run.py final-filtering -v /users/ludwig/bgn494/sharedscratch/barretts_sc_project/variant_calling_with_pysam/scrnaseqcaller_output/output_files/${1}_Hisat2_filtered.bed -o /users/ludwig/bgn494/sharedscratch/barretts_sc_project/variant_calling_with_pysam/scrnaseqcaller_output/output_files/${1}_Hisat2_final 


# End of job script
