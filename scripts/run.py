#!/usr/bin/env python

import click
import modules.preprocessing as preprocessing
import modules.remove_rt_errors as remove_rt_errors
import modules.final_filtering as final_filtering

@click.group()
def cli():
    """
    scRNAseqVariantCaller
    """
    pass


cli.add_command(preprocessing.calculate_parameters)
cli.add_command(remove_rt_errors.remove_reverse_transcriptase_errors)
cli.add_command(final_filtering.final_filtering)



if __name__ == '__main__':
    cli()